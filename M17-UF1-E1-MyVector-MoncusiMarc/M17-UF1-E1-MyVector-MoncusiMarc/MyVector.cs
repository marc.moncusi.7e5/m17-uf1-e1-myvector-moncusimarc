﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_MoncusiMarc
{
    class MyVector : IComparable
    {
        private double[][] vector = new double[2][];

        public MyVector(double[] inici, double[] final)
        {
            vector[0] = inici;
            vector[1] = final;
        }

        public double[][] GetVector()
        {
            return vector;
        }
        public void SetVector(double[] inici, double[] fi)
        {
            vector[0] = inici;
            vector[1] = fi;
        }
        public double Longitud()
        {
            return Math.Sqrt(Math.Pow(vector[1][0] - vector[0][0], 2) + Math.Pow(vector[1][1] - vector[0][1], 2));
        }

        public void Invertir()
        {
            (vector[0], vector[1]) = (vector[1], vector[0]);
        }
        public override string ToString()
        {
            return $"Inici: ({vector[0][0]},{vector[0][1]}) Fi: ({vector[1][0]},{vector[1][1]})\n" +
                $"Longitud {Longitud()} Distancia: {DistanceTo0()}";
        }

        public double DistanceTo0()
        {
            var nominator = Math.Abs((vector[1][0] - vector[0][0]) * (vector[0][1]) -
                                     (vector[0][0]) * (vector[1][1] - vector[0][1]));
            return nominator / Longitud();
        }

        public int CompareTo(object obj)
        {
            var tmp = (MyVector) obj;
            var dif = Longitud() - tmp.Longitud();
            if (dif > 0)
            {
                return 1;
            } else if (dif < 0)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
}
