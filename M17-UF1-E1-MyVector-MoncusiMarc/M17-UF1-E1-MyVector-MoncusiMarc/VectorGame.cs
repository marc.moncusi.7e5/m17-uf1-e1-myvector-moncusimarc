﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_MoncusiMarc
{
    class VectorGame
    {
        private ArrayList vectorgame = new ArrayList();

        public VectorGame()
        {

        }
        public void RandomVector(int lenght)
        {
            Random rnd = new Random();
            for (var i = 0; i < lenght; i++)
            {
                MyVector vectorTmp = new MyVector(new double[2] { rnd.Next(0,100), rnd.Next(0,100) },
                               new double[2] { rnd.Next(0,100), rnd.Next(0,100) });
                vectorgame.Add(vectorTmp);
            }
        }
        public void SortVectors(bool longitud)
        {
            if (longitud)
            {
                vectorgame.Sort(new SortByLenght());
            }
            else
            {
                vectorgame.Sort(new SortByDistance());
            }
        }

        public void Visualitzar()
        {
            foreach (var value in  vectorgame)
            {
                Console.WriteLine(value.ToString());
            }
        }

        internal class SortByLenght : IComparer
        {
            public int Compare(object x, object y)
            {
                var vx = (MyVector) x;
                var vy = (MyVector) y;
                var dif = vx!.CompareTo(vy);
                if (dif > 0)
                {
                    return 1;
                }
                else if (dif < 0)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
        }

        internal class SortByDistance : IComparer
        {
            public int Compare(object x,object y)
            {
                var vx = (MyVector) x;
                var vy = (MyVector) y;
                var dif = vx!.DistanceTo0() - vy!.DistanceTo0();
                if (dif > 0)
                {
                    return 1;
                }
                else if (dif < 0)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
