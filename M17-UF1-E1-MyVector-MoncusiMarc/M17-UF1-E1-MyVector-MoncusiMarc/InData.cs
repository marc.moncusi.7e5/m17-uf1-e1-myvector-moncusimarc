using System;

namespace M17_UF1_E1_MyVector_MoncusiMarc
{
    public static class InData
    {
        public static int InsertInt()
        {
            int cInt; 
            string cInput; 
            do
            { 
                Console.WriteLine("Introdueix un valor sencer."); 
                Console.Write("--> "); 
                cInput = Console.ReadLine();
            } while (!int.TryParse(cInput,out cInt));
            return cInt;
            
        }
    }
}