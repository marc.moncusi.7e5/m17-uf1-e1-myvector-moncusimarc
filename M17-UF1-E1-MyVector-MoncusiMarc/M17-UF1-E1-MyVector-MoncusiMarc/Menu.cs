using System;

namespace M17_UF1_E1_MyVector_MoncusiMarc
{
    public static class Menu
    {
        public static void MainMenu()
        {
            Console.WriteLine("------ MENU PRINCIPAL ------");
            Console.WriteLine("");
            Console.WriteLine("1. VectorGame ordenat per longitud");
            Console.WriteLine("2. VectorGame ordenat per distancia");
            Console.WriteLine("3. MyVector");
            Console.WriteLine("0. Sortir");
            Console.WriteLine("");
            Console.WriteLine("----------------------------");
        }
    }
}