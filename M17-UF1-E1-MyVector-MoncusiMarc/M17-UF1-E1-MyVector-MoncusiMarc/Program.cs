﻿using System;

namespace M17_UF1_E1_MyVector_MoncusiMarc
{
    class Program
    {
        static void Main(string[] args)
        {
            HelloName();
            int ex;
            do
            {
                Menu.MainMenu();
                Console.WriteLine("Escull un numero a executar");
                ex = InData.InsertInt();
                Console.Clear();
                int len;
                VectorGame vg;
                switch (ex)
                {
                    case 1:
                        vg = new VectorGame();
                        Console.WriteLine("Numero de vectors?");
                        len = InData.InsertInt();
                        vg.RandomVector(len);
                        vg.SortVectors(true);
                        vg.Visualitzar();
                        break;
                    case 2:
                        vg = new VectorGame();
                        Console.WriteLine("Numero de vectors?");
                        len = InData.InsertInt();
                        vg.RandomVector(len);
                        vg.SortVectors(false);
                        vg.Visualitzar();
                        break;
                    case 3:
                        Console.WriteLine("Creem un vector amb inici a 1,1 i fi a 3,4");
                        MyVector vector = new MyVector(new double[2] { 1, 1 },
                               new double[2] { 3, 4 });
                        Console.Write("Get: ");
                        var temp = vector.GetVector();
                        Console.WriteLine($"({temp[0][1]},{temp[0][1]}), ({temp[1][0]},{temp[1][1]})");
                        Console.WriteLine("Set: Cambiem el primer punt a 2,2");
                        vector.SetVector(new double[2] { 2, 2 },
                               new double[2] { 3, 4 });
                        Console.WriteLine("Distancia: " + vector.Longitud());
                        Console.WriteLine("Cambi de direcció:");
                        vector.Invertir();
                        Console.WriteLine("Imprimim: " + vector.ToString());
                        break;
                    case 0:
                        break;
                    default:
                        Console.WriteLine("no s'ha trobat la comanda");
                        break;
                }
            } while (ex != 0);
        }
        static void HelloName()
        {
            Console.Write("Escriu el teu nom: ");
            Console.WriteLine("Hello " + Console.ReadLine());
        }
    }
}
